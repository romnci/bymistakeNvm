package land.chipmunk.chipmunkmod.data;

public class ChomeNSBotCommand {
    public final String name;
    public final TrustLevel trustLevel;

    public ChomeNSBotCommand (
            String name,
            TrustLevel trustLevel
    ) {
        this.name = name;
        this.trustLevel = trustLevel;
    }

    public enum TrustLevel {
        PUBLIC,
        TRUSTED,
        OWNER
    }
}
