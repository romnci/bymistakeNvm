package land.chipmunk.chipmunkmod.modules;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.data.ChomeNSBotCommand;
import land.chipmunk.chipmunkmod.listeners.Listener;
import land.chipmunk.chipmunkmod.listeners.ListenerManager;
import land.chipmunk.chipmunkmod.util.UUIDUtilities;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.text.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChomeNSBotCommandSuggestions extends Listener {
    public static final String ID = "chomens_bot_request_command_suggestion";

    public static ChomeNSBotCommandSuggestions INSTANCE = new ChomeNSBotCommandSuggestions(MinecraftClient.getInstance());

    private final MinecraftClient client;

    public List<ChomeNSBotCommand> commands = new ArrayList<>();

    public ChomeNSBotCommandSuggestions (MinecraftClient client) {
        this.client = client;

        ListenerManager.addListener(this);
    }

    public void init () {}

    @Override
    public void coreMoved () { forceRequest(); }

    public void forceRequest () {
        final ClientPlayerEntity player = client.player;

        if (player == null) return;

        final String selector = UUIDUtilities.selector(player.getUuid());

        final Component component = Component
                .text(ID)
                .append(Component.text(selector));

        final String serialized = GsonComponentSerializer.gson().serialize(component);

        CommandCore.INSTANCE.run("tellraw @a[tag=chomens_bot] " + serialized);
    }

    @Override
    public void chatMessageReceived(Text message) {
        try {
            final Component component = message.asComponent();

            final List<Component> children = component.children();

            if (children.size() == 0) return;

            final TextComponent textComponent = (TextComponent) children.get(0);

            if (!textComponent.content().equals(ID)) return;

            commands = children.subList(1, children.size())
                    .stream()
                    .map(
                            (eachCum) -> new ChomeNSBotCommand(
                                    ChipmunkMod.CONFIG.bots.chomens.prefix + ((TextComponent) eachCum).content(),
                                    ChomeNSBotCommand.TrustLevel.valueOf(((TextComponent) eachCum.children().get(0)).content())
                            )
                    )
                    .toList();
        } catch (Exception ignored) {}
    }
}
