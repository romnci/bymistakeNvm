package land.chipmunk.chipmunkmod.util;

import com.mojang.brigadier.Command;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.Configuration;
import land.chipmunk.chipmunkmod.commands.SayCommand;
import land.chipmunk.chipmunkmod.modules.Chat;
import land.chipmunk.chipmunkmod.modules.CustomChat;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class BotValidationUtilities {
    public static int hbot (String command) throws RuntimeException {
        final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.hbot;
        final MinecraftClient client = MinecraftClient.getInstance();
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String time = String.valueOf(System.currentTimeMillis() / 10000);
            String input = prefix + command.replaceAll("&[0-9a-fklmnor]", "") + ";" + client.player.getUuidAsString() + ";" + time + ";" + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
            String stringHash = bigInt.toString(Character.MAX_RADIX);

            Chat.sendChatMessage(prefix + command + " " + stringHash, true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int sbot (String command) throws RuntimeException {
        final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.sbot;
        final MinecraftClient client = MinecraftClient.getInstance();
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String time = String.valueOf(System.currentTimeMillis() / 20000);
            String input = prefix + command.replaceAll("&[0-9a-fklmnorx]", "") + ";" + client.player.getName() + ";" + time + ";" + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
            String stringHash = bigInt.toString(Character.MAX_RADIX);

            Chat.sendChatMessage(prefix + command + " " + stringHash, true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int chomens (String command) throws RuntimeException {
        final Configuration.ChomeNSBotInfo info = ChipmunkMod.CONFIG.bots.chomens;

        final MinecraftClient client = MinecraftClient.getInstance();

        final ClientPlayerEntity player = client.player;

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            String[] arguments = command.split(" ");

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String time = String.valueOf(System.currentTimeMillis() / 5_000);
            String input = client.player.getUuidAsString() + arguments[0] + time + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            String stringHash = Hexadecimal.encode(hash).substring(0, 16);

            final boolean shouldSectionSign = CustomChat.INSTANCE.enabled && player.hasPermissionLevel(2) && player.isCreative();

            if (shouldSectionSign) {
                stringHash = String.join("",
                        Arrays.stream(stringHash.split(""))
                            .map((letter) -> "§" + letter)
                            .toArray(String[]::new)
                );
            }

            final String[] restArguments = Arrays.copyOfRange(arguments, 1, arguments.length);

            final String toSend = prefix +
                    arguments[0] +
                    " " +
                    stringHash +
                    (shouldSectionSign ? "§r" : "") +
                    " " +
                    String.join(" ", restArguments);

            Chat.sendChatMessage(toSend);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int kittycorp (String command) throws RuntimeException {
        final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.kittycorp;
        final ClientPlayNetworkHandler networkHandler = MinecraftClient.getInstance().getNetworkHandler();

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String time = String.valueOf(System.currentTimeMillis() / 10000);
            String input = prefix + command.replaceAll("&[0-9a-fklmnorx]", "") + ";" + time + ";" + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
            String stringHash = bigInt.toString(Character.MAX_RADIX);

            Chat.sendChatMessage(prefix + command + " " + stringHash, true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }
}
