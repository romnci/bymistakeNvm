package land.chipmunk.chipmunkmod.mixin;

import net.minecraft.nbt.*;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.io.DataInput;
import java.io.IOException;

@Mixin(NbtIo.class)
public class NbtIoMixin {
    @Inject(method = "read(Ljava/io/DataInput;ILnet/minecraft/nbt/NbtTagSizeTracker;)Lnet/minecraft/nbt/NbtElement;", at = @At("HEAD"), cancellable = true)
    private static void read(DataInput input, int depth, NbtTagSizeTracker tracker, CallbackInfoReturnable<NbtElement> cir) {
        try {
            byte b = input.readByte();
            if (b == 0) {
                cir.setReturnValue(NbtEnd.INSTANCE);
            }

            NbtString.skip(input);
            cir.setReturnValue(NbtTypes.byId(b).read(input, depth, tracker));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
