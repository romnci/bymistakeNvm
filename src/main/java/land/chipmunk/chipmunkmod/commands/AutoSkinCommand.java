package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import land.chipmunk.chipmunkmod.modules.SelfCare;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.StringArgumentType.string;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;

public class AutoSkinCommand {
    public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
        dispatcher.register(
                literal("autoskin")
                        .then(
                                argument("username", string()) // i don't think theres an account with space(s)
                                        .executes(AutoSkinCommand::execute)
                        )
        );
    }

    public static int execute(CommandContext<FabricClientCommandSource> context) {
        final FabricClientCommandSource source = context.getSource();

        final String username = getString(context, "username");

        SelfCare.INSTANCE.skin = username;

        if (username.equals("off")) source.sendFeedback(Text.literal("Successfully disabled auto skin"));
        else {
            SelfCare.INSTANCE.hasSkin = false;
            source.sendFeedback(Text.literal("Set your auto skin username to: " + username));
        }

        return Command.SINGLE_SUCCESS;
    }
}
