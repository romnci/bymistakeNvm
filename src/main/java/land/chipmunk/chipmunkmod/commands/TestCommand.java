package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

public class TestCommand {
  public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
    dispatcher.register(
      literal("test")
        .executes(c -> helloWorld(c))
    );
  }

  public static int helloWorld (CommandContext<FabricClientCommandSource> context) {
    final FabricClientCommandSource source = context.getSource();
    source.sendFeedback(Text.literal("Hello, world!"));

    return Command.SINGLE_SUCCESS;
  }
}
