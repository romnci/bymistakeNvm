package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import land.chipmunk.chipmunkmod.modules.SelfCare;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

import static com.mojang.brigadier.arguments.BoolArgumentType.bool;
import static com.mojang.brigadier.arguments.BoolArgumentType.getBool;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;

public class SelfCareCommand {
    public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
        dispatcher.register(
                literal("selfcare")
                        .then(
                                literal("op")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "op"))
                                        )
                        )
                        .then(
                                literal("gamemode")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "gamemode"))
                                        )
                        )
                        .then(
                                literal("cspy")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "cspy"))
                                        )
                        )
                        .then(
                                literal("icu")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "icu"))
                                        )
                        )
        );
    }

    // setSelfCare is probably not a good name for this
    public static int setSelfCare (CommandContext<FabricClientCommandSource> context, String type) {
        final FabricClientCommandSource source = context.getSource();
        final boolean bool = getBool(context, "boolean");

        switch (type) {
            case "op" -> {
                SelfCare.INSTANCE.opEnabled = bool;
                source.sendFeedback(Text.literal("The op self care is now " + (bool ? "enabled" : "disabled")));
            }
            case "gamemode" -> {
                SelfCare.INSTANCE.gamemodeEnabled = bool;
                source.sendFeedback(Text.literal("The gamemode self care is now " + (bool ? "enabled" : "disabled")));
            }
            case "cspy" -> {
                SelfCare.INSTANCE.cspyEnabled = bool;
                source.sendFeedback(Text.literal("The CommandSpy self care is now " + (bool ? "enabled" : "disabled")));
            }
            case "icu" -> {
                SelfCare.INSTANCE.icuEnabled = bool;
                source.sendFeedback(Text.literal("The iControlU self care is now " + (bool ? "enabled" : "disabled")));
            }
        }

        return Command.SINGLE_SUCCESS;
    }
}
